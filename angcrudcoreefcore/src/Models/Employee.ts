export class Employee {  
    employeeID :number;
    lastName :string;
    firstName :string;
    title :string;
     birthDate :Date;
     address :string;
    city :string;
    region :string;
     postalCode :string;
     country :string; 
} 