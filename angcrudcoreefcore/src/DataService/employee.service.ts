import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';  
import { Injectable } from '@angular/core';  
import { Employee } from 'src/Models/Employee'  
import { ROOT_URL } from 'src/Models/Config'  
import { Observable } from 'rxjs';  
@Injectable({
  providedIn: 'root'
})  
export class EmployeeDataService {  
  employees: Observable<Employee[]>;  
  newemployee: Employee; 
  url='http://localhost:54905/api' ;
  constructor(private http: HttpClient) {  
  
  }  
  
  getEmployee() {  
    return this.http.get<Employee[]>(this.url + '/employees');  
  }  
  AddEmployee(emp: Employee) {  
  
    const headers = new HttpHeaders().set('content-type', 'application/json');  
    var body = {  
      lastname: emp.lastName, firstname: emp.firstName, title: emp.title, birthDate: emp.birthDate,  
      address: emp.address, city: emp.city, region: emp.region, postalCode: emp.postalCode, country:emp.country
    }  
    console.log(ROOT_URL);  
  
    return this.http.post<Employee>(ROOT_URL + '/Employees', body, { headers });  
  
  }  
  
  ///  
  EditEmployee(emp: Employee) {  
    console.log(emp);  
    //const params = new HttpParams().set('ID', emp.employeeID);  
    const headers = new HttpHeaders().set('content-type', 'application/json');  
    var body = {  
      lastname: emp.lastName, firstname: emp.firstName, title: emp.title, birthDate: emp.birthDate,  
      address: emp.address, city: emp.city, region: emp.region, postalCode: emp.postalCode, country:emp.country
    }  
    return this.http.put<Employee>(ROOT_URL + '/Employees/' + emp.employeeID, body, {headers})
    // return this.http.put<Employee>(ROOT_URL + 'Employees/' + emp.employeeID, body, { headers, params })  
  
  }  
  DeleteEmployee(emp: Employee) {  
    //const params = new HttpParams().set('ID', emp.employeeID);  
    const headers = new HttpHeaders().set('content-type', 'application/json');  
    var body = {  
      lastname: emp.lastName, firstname: emp.firstName, title: emp.title, birthDate: emp.birthDate,  
      address: emp.address, city: emp.city, region: emp.region, postalCode: emp.postalCode, country:emp.country
    }  
    return this.http.delete<Employee>(ROOT_URL + '/Employees/' + emp.employeeID)  
  
  }  
  
}  