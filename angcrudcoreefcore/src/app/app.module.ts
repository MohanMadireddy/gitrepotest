import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { EmployeeAddComponent } from './employee-add/employee-add.component';

import { AngularCRUDComponent } from './angular-crud/angular-crud.component';
import { EmployeeupdateComponent } from './employeupdate/employeupdate.component';
import { FormsModule } from '@angular/forms';
import { EmployeeDataService } from 'src/DataService/employee.service';

@NgModule({
  declarations: [
    AppComponent,
    AngularCRUDComponent,
    EmployeeAddComponent,
    EmployeeupdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, FormsModule
  ],
  providers: [EmployeeDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
