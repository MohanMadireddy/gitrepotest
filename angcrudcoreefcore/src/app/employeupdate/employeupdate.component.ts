import { Component, OnInit, ViewChild, Input, EventEmitter, Output, ElementRef } from '@angular/core';  

import { Router } from '@angular/router';  
import { NgForm } from '@angular/forms';  
import { Employee } from 'src/Models/Employee';  
import { EmployeeDataService } from 'src/DataService/employee.service';
@Component({  
  selector: 'app-employeeupdate',  
  templateUrl:'./employeupdate.component.html',

  styleUrls: ['./employeupdate.component.css']  
})  
export class EmployeeupdateComponent implements OnInit  
 {  
  constructor(private dataservice:EmployeeDataService,private route:Router) {  
  }  
  @Output() nameEvent = new EventEmitter<string>();  
  @ViewChild('closeBtn',{static: false}) cb: ElementRef;  
  ngOnInit() {  
  }  
  @Input() reset:boolean = false;  
  @ViewChild('regForm',{static: false}) myForm: NgForm;  
  @Input() isReset: boolean = false;  
  objtempemp:Employee;  
  @Input() objemp :Employee=new Employee();  
  EditEmployee(regForm:NgForm)  
  {  
    this.dataservice.EditEmployee(this.objemp).subscribe(res=>  
      {  
      alert("Employee updated successfully");  
      this.nameEvent.emit("ccc");  
      this.cb.nativeElement.click();  
       
      })  
  };  
}