import { Component, OnInit, ViewChild } from '@angular/core';  
import { EmployeeAddComponent } from '../employee-add/employee-add.component';  

import { Employee } from 'src/Models/Employee'  
import { Router } from '@angular/router';  

import { EmployeeDataService } from 'src/DataService/employee.service';
import { EmployeeupdateComponent } from '../employeupdate/employeupdate.component';
@Component({  
  selector: 'app-angular-crud',  
  templateUrl: './angular-crud.component.html',  
  styleUrls: ['./angular-crud.component.css']  
})  
export class AngularCRUDComponent implements OnInit {  
  
  emplist: Employee[];  
  dataavailbale: Boolean = false;  
  tempemp: Employee  
  
  constructor(private dataservce:EmployeeDataService, private route: Router) {  
  }  
  ngOnInit() {  
    this.LoadData();  
  }  
  LoadData() {  
    this.dataservce.getEmployee().subscribe((tempdate) => {  
      this.emplist = tempdate;  
      console.log(this.emplist);  
      if (this.emplist.length > 0) {  
        this.dataavailbale = true;  
      }  
      else {  
        this.dataavailbale = false;  
      }  
    }  
    )  
      , err => {  
        console.log(err);  
      }  
  }  
  deleteconfirmation(id: number) {  
  
    if (confirm("Are you sure you want to delete this ?")) {  
      this.tempemp = new Employee();  
      this.tempemp.employeeID = id;  
      this.dataservce.DeleteEmployee(this.tempemp).subscribe(res => {  
        alert("Deleted successfully !!!");  
        this.LoadData();  
      })  
    }  
  }  
  @ViewChild('empadd',{static: false}) addcomponent: EmployeeAddComponent  
  @ViewChild('regForm',{static: false}) editcomponent:EmployeeupdateComponent  
  loadAddnew() {  
    this.addcomponent.objemp.firstName = ""  
    this.addcomponent.objemp.lastName = ""  
    this.addcomponent.objemp.title = ""  
   // this.addcomponent.objemp.birthDate = ""  
    this.addcomponent.objemp.address = "" 
    this.addcomponent.objemp.city = ""
    this.addcomponent.objemp.region = ""
    this.addcomponent.objemp.postalCode = ""
    this.addcomponent.objemp.country = ""
     
  }  
  loadnewForm(employee :Employee)
  {
    //employeeID: string, firstname: string, lastname: string, title: string,birthDate:Date,address:string,, gender: number) {  
    //console.log(gender);  
    this.editcomponent.objemp.firstName = employee.firstName  
    this.editcomponent.objemp.lastName = employee.lastName  
    this.editcomponent.objemp.title = employee.title  
    this.editcomponent.objemp.birthDate = employee.birthDate  
    this.editcomponent.objemp.address = employee.address  
    this.editcomponent.objemp.city = employee.city  
    this.editcomponent.objemp.region = employee.region  
    this.editcomponent.objemp.postalCode = employee.postalCode  
    this.editcomponent.objemp.country = employee.country
  }  
  RefreshData() {  
    this.LoadData();  
  }  
}  