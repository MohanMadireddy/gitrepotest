import { Component, OnInit,Input, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';  
import { NgForm } from '@angular/forms';  
import { Employee } from 'src/Models/Employee';  
import { Router } from '@angular/router';  
 
import { EmployeeDataService } from 'src/DataService/employee.service';
@Component({  
  selector: 'app-employee-add',  
  templateUrl: './employee-add.component.html',  
  styleUrls: ['./employee-add.component.css']  
})  
export class EmployeeAddComponent implements OnInit {  
    
  @Input()  cleardata: boolean = false;  
  @Output() nameEvent = new EventEmitter<string>();  
  objtempemp:Employee;  
  @Input() objemp :Employee=new Employee();  
  @ViewChild('closeBtn',{static: false}) cb:ElementRef;
 
constructor(private dataservice:EmployeeDataService,private route:Router) {  
   
 }  
 ngOnInit() {  
  }  
 Register(regForm:NgForm){    
    this.objtempemp=new Employee();  
    this.objtempemp.lastName=regForm.value.lastName;  
    this.objtempemp.firstName=regForm.value.firstName;  
    this.objtempemp.title=regForm.value.title;  
    this.objtempemp.birthDate=regForm.value.birthDate;  
    this.objtempemp.address=regForm.value.address;
    this.objtempemp.city=regForm.value.city;  
    this.objtempemp.region=regForm.value.regForm;  
    this.objtempemp.postalCode=regForm.value.postalCode;  
    this.objtempemp.country=regForm.value.country;
      
  this.dataservice.AddEmployee(this.objtempemp).subscribe(res=>{  
    alert("Employee Added successfully");  
    this.TakeHome();  
}  
  )  
  }   
  TakeHome(){  
    this.nameEvent.emit("ccc");  
    this.cb.nativeElement.click();  
    this.route.navigateByUrl('');  
  }  
}  