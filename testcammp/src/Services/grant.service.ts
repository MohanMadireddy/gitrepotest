import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GrantSet } from 'src/Models/Grantset';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
//import {Root_url} from './Config/ROOT_URL';

@Injectable({
  providedIn: 'root'
})
export class GrantserviceService {
grants: Observable<GrantSet[]>;
url = "http://localhost:62723/api";
  constructor(private http:HttpClient) { }
  private grantset : GrantSet[];
  getGrants() : Observable<GrantSet[]>
  {
   /*  return this.http.get(this.url+'/GrantSets').pipe(map(response =>{const grantset = response.json();
      return this.grantset.map((res) => new GrantSet(res));
    }).catchError(this.handleError)); */
   //<GrantSet[]>response.json());.catch(this.handleError);
   return this.http.get<GrantSet[]>(this.url+'/GrantSets')
   
  }

/*   private extractData(res: Response) {
    let body = res.json();
    return body.data || {};
}*/

 /* private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server Error');
} */
}
