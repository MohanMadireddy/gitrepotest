export class GrantSet{
    Status : string;
    Grant_start: Date;
    Grant_End:Date;
    update_date:Date;
    Grantid: string;
    GrantName: string;
    ProgramName:string;
    GrantTypeId : string;
    GrantSetFiscalYear:string;
}