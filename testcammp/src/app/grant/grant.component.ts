import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { GrantSet } from 'src/Models/Grantset';

import { map } from 'rxjs/operators';
import { GrantserviceService } from 'src/Services/grant.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-grant',
  templateUrl: './grant.component.html',
  styleUrls: ['./grant.component.css']
})
export class GrantComponent implements OnInit {
 private grantsets=[];

selected: GrantSet[] = []

  allGrantsets : any; //Observable<GrantSet[]>
  constructor(private grantservice:GrantserviceService,private http:HttpClient) { }

  //@ViewChild("download",{static:false})   downloadLink: ElementRef;
  
  ngOnInit() {
    //this.http.get('http://localhost:62723/api' + '/GrantSets').subscribe(res=>console.log(res));
     this.grantservice.getGrants().subscribe((res:any[]) =>{this.grantsets = res;});
     //this.grantservice.getGrants().subscribe(res=>this.allGrantsets = res);
  }
 ToExcel()
  {
    //const excelData = this.allGrantsets.pipe(map(grant=> this.grantsets).join("\n"));
   // this.grantservice.getGrants().subscribe((res:any[]) =>{this.grantsets = res as GrantSet[];});
  //this.ConvertToCSV(data);

  var csvData = this.ConvertToCSV(this.grantsets);
                        var a = document.createElement("a");
                        a.setAttribute('style', 'display:none;');
                        document.body.appendChild(a);
                        var blob = new Blob([csvData], { type: 'text/csv' });
                        var url= window.URL.createObjectURL(blob);
                        a.href = url;
                        a.download = 'Grant.csv';/* your file name*/
                        a.click();
                        return 'success';




  //this.renderer.setAttribute(this.downloadLink.nativeElement, "href",
  //    "data:text/plain;charset=utf-8,"+data);
  }

  ConvertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
      var str = '';
    var row = "";

    for (var index in objArray[0]) {
        //Now convert each value to string and comma-separated
        row += index + ',';
    }
    row = row.slice(0, -1);
    //append Label row with line break
    str += row + '\r\n';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }
        str += line + '\r\n';
    }
    return str;
}
}
