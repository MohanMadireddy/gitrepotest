import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GrantComponent } from './grant/grant.component';
import { AppComponent } from './app.component';


const routes: Routes = [
  //{path:'/', component:AppComponent},
  {path:'GrantSet',component:GrantComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
